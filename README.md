## 使用情景

安卓手机调用百度ocr识别文字

## 效果展示



## 代码讲解

1. `nodejs`运行代码, 获取`token`

```js
const param = qs.stringify({
  grant_type: "client_credentials",
  client_id: AK,
  client_secret: SK,
});

https.get(
  {
    hostname: "aip.baidubce.com",
    path: "/oauth/2.0/token?" + param,
    agent: false,
  },
  function (res) {
    // 在标准输出中查看运行结果
    res.pipe(process.stdout);
  }
);
```

2. 遍历识别图片中的文字

```js
for (var i = 0; i < len; i++) {
  let imgPath = imgPathList[i];
  let dataList = ocr(imgPath);
  showData(dataList, imgPath, "百度");
}
```

3. 使用showData函数, 用canvas在图片上画出识别结果, 然后保存到手机

```js
canvas.drawRect(
  location.left,
  location.top,
  location.left + location.width,
  location.top + location.height,
  rectanglePaint
);
canvas.drawText(
  data.words,
  location.left + parseInt(location.width / 2),
  location.top + location.height + Math.abs(fontMetrics.top),
  textPaint
);
images.save(image, newFilepath);
```

---

## 代码仓库:

https://gitee.com/yashujs/autojs-ocr-baidu

---



## 微信公众号 AutoJsPro教程

![](公众号.jpg)

## QQ群

747748653

![](D:\自媒体\autojs\autojs\给我个面子小图.jpg)