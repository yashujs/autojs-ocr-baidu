function showData(dataList, imgPath, ocrType) {
  log("显示数据 imgPath = " + imgPath);
  var img = images.read(imgPath);
  var canvas = new Canvas(img);
  let rectanglePaint = new Paint();
  rectanglePaint.setStrokeWidth(3);
  rectanglePaint.setColor(colors.parseColor("#00ff00"));
  rectanglePaint.setStyle(Paint.Style.STROKE); //空心矩形框

  let textPaint = new Paint();
  textPaint.setTextAlign(Paint.Align.CENTER);
  textPaint.setTextSize(30);
  textPaint.setStyle(Paint.Style.FILL);
  textPaint.setColor(colors.parseColor("#f000ff"));
  let fontMetrics = textPaint.getFontMetrics();

  var len = dataList.length;
  for (var i = 0; i < len; i++) {
    let data = dataList[i];
    let location = data.location;
    canvas.drawRect(
      location.left,
      location.top,
      location.left + location.width,
      location.top + location.height,
      rectanglePaint
    );
    canvas.drawText(
      data.words,
      location.left + parseInt(location.width / 2),
      location.top + location.height + Math.abs(fontMetrics.top),
      textPaint
    );
  }
  var image = canvas.toImage();
  let newFilename = files.getNameWithoutExtension(imgPath) + "_" + ocrType + ".png";
  let newFilepath = "/sdcard/脚本/" + ocrType + "/" + newFilename;
  files.createWithDirs(newFilepath);
  images.save(image, newFilepath);
  log("识别后的图片保存路径: " + newFilepath);
  img.recycle();
}

events.on("exit", function () {
  log("结束运行 模块脚本");
});

module.exports = showData;
