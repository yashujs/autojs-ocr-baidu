// nodejs运行
var https = require("https");
var qs = require("querystring");
console.log("qs =");
console.log(qs);
console.log("https =");
console.log(https);

// 指应用的ak和sk, 请不要使用右上角那个, 不是那个
const AK = "xxxxxxxxx";
const SK = "xxxxxxxxx";

const param = qs.stringify({
  grant_type: "client_credentials",
  client_id: AK,
  client_secret: SK,
});

https.get(
  {
    hostname: "aip.baidubce.com",
    path: "/oauth/2.0/token?" + param,
    agent: false,
  },
  function (res) {
    // 在标准输出中查看运行结果
    res.pipe(process.stdout);
  }
);

// 服务器返回的JSON文本参数如下：

// access_token： 要获取的Access Token；
// expires_in： Access Token的有效期(秒为单位，一般为1个月)；
// 其他参数忽略，暂时不用;
